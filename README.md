# Practica de balanceo de cargas de alta disponibilidad 

Se van a crear cinco servidores diferentes (bastion, servidor 1, servidor 2, servidor 3 y servidor frontal) utilizando contenedores con imágenes de Ubuntu y NGINX.

## Como funciona:

Primero se configura un archivo **(docker-compose.yml)** en donde se crean los 5 servidores que la practica requiere. 
El primer servidor es el bastion el cual configure dejando el puerto 22 abierto y cambiando el password de root para poder acceder desde cualquier computadora, porque luego la anda haciendo de tos.

También se crea un archivo dockerfile para llamado **(DockerfileBas)** Se instala todo lo necesario para poder hacer correr los servidores: (Antes que todo hay que hacer un apt update)
RUN apt update 
RUN apt install -y openssh-server
RUN apt install -y ansible 
RUN apt install -y python3
RUN apt install -y nano

RUN mkdir /etc/ansible && touch /etc/ansible/hosts

#Reestablecemos el password del usuario root
RUN echo "root:123456" | chpasswd
# Eliminamos el archivo de configuracion de SSH
RUN rm /etc/ssh/sshd_config
# Creamos el archivo de configuracion de SSH para que nos permita conectarnos como root
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
# Creamos el archivo con las llaves publicas autorizadas
RUN ssh-keygen -t rsa -f /root/.ssh/id_rsa -N ''
# Generamos nuestras llaves
RUN touch /root/.ssh/authorized_keys
# Abrimos el puerto 22 para SSH
EXPOSE 22
# Arrancamos el servicio SSH 
CMD service ssh start ; sleep infinity

```

Los siguientes fueron los 3 servidores los cuales también les configure el nombre server-1,server-2,server-3 un punto importante es el activar el privileged: true, ya que esto es lo que les va a permitir ejecutar docker.

Para estos servidores también configure un archivo dockerfile el cual se llama **(DockerfileServ)** aquí instalo todo lo necesario para su funcionamiento básico, también cambió la contraseña de root y activo el inicio de sesión de root. También creó el archivo authorized_keys y dejó iniciado el servicio ssh.

```
# Tomamos la imagen de ubuntu mas reciente
FROM ubuntu:latest
WORKDIR /root
# Instalamos SSH, Sudo, Python3, nano y git

RUN echo "root:123456" | chpasswd
RUN rm /etc/ssh/sshd_config

RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config

# Creamos el archivo con las llaves publicas autorizadas
RUN mkdir /root/.ssh && touch /root/.ssh/authorized_keys
# Arrancamos el servicio SSH
CMD service ssh start ; sleep infinity


```

Por ultimo configuramos el servidor frontal con NGINX

```
  balanceador:
    image: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    depends_on:
      - server-1
      - server-2
      - server-3

```
## Configuracion mediante ansible


Ahora hace falta hacer la configuración de ansible, empezamos por conectarnos al bastion:

```bash
ssh root@bastion
```

Hay que ocnfigurar los hosts, a los cuales se van a conectar:

```
localhost ansible_connection=local 

serv1 ansible_user=root
serv2 ansible_user=root
serv3 ansible_user=root 

 


```
Despues se saca la llave PUBLICA, no la privada, y se le instala en .ssh/authorized_keys de cada servidor.
Con todos los servidores configurados lanze un comando ansible para comprobar que todo esté bien.

```bash
ansible all -a "ls"
```

Ya con todo esto podemos hacer el **(playbookDocker.yml)** el cual contiene lo necesario para instalar docker, clonar el repositorio git con la aplicación de balanceo y ejecutar docker compose en cada uno de ellos.

```
- hosts: ['serv1', 'serv2', 'serv3']
  become: yes
  tasks:
# Instalamos Curl
  - name: Instalamos curl
    apt:
      name: curl
      state: present
  - name: Instalamos Docker
    shell: 'curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh'

# Iniciamos docker 
  - name: Iniciamos el servicio de Docker por medio del shell
    shell: 'sudo service docker start'
    ignore_errors: yes

# Clonamos la apicacion de balanceo 
  - name: Clonamos el repositorio de la aplicación de balanceo
    git:
      repo: https://gitlab.com/eduardorc190299/balancing-app
      dest: /home/balancing-app


# Ejecutamos el docker compose
- hosts: ['serv1', 'serv2', 'serv3']
  become: yes
  tasks:
  - name: Acceder a la carpeta con la aplicación de balanceo y ejecutar docker compose
    shell: 'cd /home/balancing-app && docker compose up -d'

```
Comando para ejecutar el playbook

```bash
ansible-playbook playbookDocker.yml
```

Y listo.
